var express = require('express');
var app = express();

var connector = require('./connectors/db-connector');
var tohService = require('./services/toh');

var PORT = 3000;

connector.getConnector();

app.get('/', function(req, res) {
    res.status(200).send('Hello world');
});

app.get('/heroes', async function(req, res) {
  const result = await tohService.getHeroes();
  console.log('RESULT', result);
  res.status(200).send(result);
})

app.listen(PORT, function() {
    console.log('Server is running on PORT:',PORT);
});
