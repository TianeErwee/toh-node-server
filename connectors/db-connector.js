const mysql = require('mysql');

const getConnector = () => {
  const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'toh'
  });
  
  db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
  });

  global.db = db;
}

module.exports = { getConnector };