const getHeroes = async(connector) => {
  const query = connector.query('SELECT * FROM `heroes`');
  return query;
}

module.exports = {getHeroes};
